package com.store.ui.component;

import com.store.enums.ThemeEnum;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.dom.ThemeList;
import com.vaadin.flow.theme.lumo.LumoUtility;
import org.vaadin.addons.taefi.component.ToggleButtonGroup;

import java.util.List;

public class ChangeThemeToggle extends ToggleButtonGroup<ThemeEnum> {

    public ChangeThemeToggle() {
        setItems(List.of(ThemeEnum.values()));
        setItemIconGenerator(align -> switch (align) {
            case LIGHT -> VaadinIcon.SUN_O.create();
            case DARK -> VaadinIcon.MOON_O.create();
        });
        addValueChangeListener(event -> {
            ThemeList themeList = UI.getCurrent().getElement().getThemeList();
            themeList.clear();
            themeList.add(event.getValue().getTheme());
        });
        setValue(ThemeEnum.DARK);
        setToggleable(false);
        addClassNames(LumoUtility.Margin.MEDIUM, LumoUtility.Padding.Vertical.NONE, LumoUtility.Padding.Horizontal.MEDIUM);
    }
}
