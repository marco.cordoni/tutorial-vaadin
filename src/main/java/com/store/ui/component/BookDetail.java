package com.store.ui.component;

import com.store.dto.BookDTO;
import com.store.service.BookService;
import com.store.service.SecurityService;
import com.store.ui.utils.ResourceUtils;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.theme.lumo.LumoUtility;

import java.math.BigDecimal;

public class BookDetail extends VerticalLayout {

    private BookDTO book;
    private Binder<BookDTO> binder;
    private FormLayout formLayout;

    private BookService bookService;

    public BookDetail(BookService bookService, SecurityService securityService, BookDTO book) {
        this.book = book;
        this.bookService = bookService;

        HorizontalLayout content = new HorizontalLayout();

        setMaxWidth("50%");

        // Add the book image
        var image = new Image(
                ResourceUtils.createStreamResource(book.getTitle(), book.getImage()),
                book.getTitle()
        );
        image.setWidth("300px");
        image.setHeight("400px");

        content.add(image);

        boolean isAdmin = securityService.isAdmin();
        formLayout = createFormFields(isAdmin);
        content.add(formLayout);

        add(content);

        if (isAdmin) {
            add(createSaveButton(isAdmin));
        }

        // Center the content
        setJustifyContentMode(JustifyContentMode.CENTER);
        setDefaultHorizontalComponentAlignment(Alignment.CENTER);
    }

    private FormLayout createFormFields(boolean isAdmin) {
        TextField title = new TextField("Title");
        title.setValue(book.getTitle());
        title.setReadOnly(!isAdmin);

        TextField author = new TextField("Author");
        author.setValue(book.getAuthor());
        author.setReadOnly(!isAdmin);

        TextField isbn = new TextField("ISBN");
        isbn.setValue(book.getIsbn());
        isbn.setReadOnly(true);

        NumberField price = new NumberField("Price €");
        price.setValue(book.getPrice().doubleValue());
        price.setReadOnly(!isAdmin);

        TextArea description = new TextArea("Description");
        description.setValue(book.getDescription());
        description.setReadOnly(!isAdmin);

        if (isAdmin) {
            binder = new Binder<>(BookDTO.class);
            binder.forField(title)
                    .asRequired("Every book must have a title")
                    .bind(BookDTO::getTitle, BookDTO::setTitle);

            binder.forField(author)
                    .asRequired("Every book must have an author")
                    .bind(BookDTO::getAuthor, BookDTO::setAuthor);

            binder.forField(isbn)
                    .asRequired("Every book must have an isbn")
                    .bind(BookDTO::getIsbn, BookDTO::setIsbn);

            binder.forField(price)
                    .asRequired("Every book must have a price")
                    .bind(
                            bookEle -> bookEle.getPrice() == null ? 0d : bookEle.getPrice().doubleValue(),
                            (bookDTO, aDouble) -> bookDTO.setPrice(BigDecimal.valueOf(aDouble))
                    );

            binder.forField(description)
                    .asRequired("Every book must have a description")
                    .bind(BookDTO::getDescription, BookDTO::setDescription);
        }

        // Add the form fields to the page
        FormLayout formLayout = new FormLayout();
        formLayout.add(title, author, isbn, price, description);

        // Setup form layout
        formLayout.setResponsiveSteps(
                // Use one column by default
                new FormLayout.ResponsiveStep("0", 1)
        );
        formLayout.setMaxWidth("500px");
        return formLayout;
    }

    private Button createSaveButton(boolean isAdmin) {
        Icon icon = new Icon(VaadinIcon.UPLOAD);
        icon.setSize("40px");
        Button button = new Button("SAVE", icon);
        button.addClassNames(
                LumoUtility.Margin.Top.MEDIUM,
                LumoUtility.FontSize.XLARGE,
                LumoUtility.Border.ALL
        );
        button.addClickListener(event -> {
            try {
                // update book
                binder.writeBean(book);
                bookService.updateBookNoImage(book);

                NotificationSuccess notificationSuccess = new NotificationSuccess("Book updated successfully");
                add(notificationSuccess);
                notificationSuccess.open();
            } catch (ValidationException e) {
                throw new RuntimeException(e);
            }
        });

        button.setEnabled(isAdmin);
        return button;
    }

}
