package com.store.ui.component;

import com.store.dto.UserDTO;
import com.store.service.SecurityService;
import com.store.service.UserService;
import com.store.ui.events.ButtonChangeUsernameEvent;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.shared.Registration;

public class LogoutButton extends Button {

    private final SecurityService securityService;
    private final UserService userService;
    private Registration registration;

    public LogoutButton(SecurityService securityService, UserService userService) {
        super("Log out " + securityService.getAuthenticatedUser().getUsername(), e -> securityService.logout());
        this.securityService = securityService;
        this.userService = userService;
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        super.onAttach(attachEvent);
        // Register to events from the event bus
        registration = ComponentUtil.addListener(
                attachEvent.getUI(),
                ButtonChangeUsernameEvent.class,
                event -> {
                    // change the button text
                    UserDTO userDTO = userService.findById(securityService.getAuthenticatedUser().getId());
                    String username = userDTO != null ? userDTO.getUsername() : "unknown user";
                    super.setText("Log out " + username);
                }
        );
    }

    @Override
    protected void onDetach(DetachEvent detachEvent) {
        super.onDetach(detachEvent);
        // Unregister from the event bus
        registration.remove();
    }
}
