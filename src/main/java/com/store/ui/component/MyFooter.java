package com.store.ui.component;

import com.vaadin.flow.component.html.Footer;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.theme.lumo.LumoUtility;

public class MyFooter extends Footer {

    public MyFooter() {
        getStyle().set("background-color", "var(--lumo-tint-10pct)");
//        getStyle().set("padding", "var(--lumo-space-m)");
        getStyle().set("text-align", "start");
        getStyle().set("position", "fixed");
        getStyle().set("left", "0");
        getStyle().set("bottom", "0");
        getStyle().set("width", "100%");

        Paragraph paragraph = new Paragraph("Marco Cordoni - 2024 - VDManga");

        paragraph.getStyle().set("margin-left", "30px");
        paragraph.getStyle().set("font-size", "10px");

        add(paragraph);
    }
}
