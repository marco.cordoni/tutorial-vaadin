package com.store.ui.view;

import com.store.ui.component.MyLoginForm;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import jakarta.annotation.security.PermitAll;

@AnonymousAllowed
@Route(value = "login", layout = MainLayout.class)
@PageTitle("VDManga | Login")
public class LoginView extends Div {

    private final LoginForm login = new LoginForm();

    public LoginView() {
        add(new MyLoginForm());
    }
}