package com.store.ui.component;

import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;

public class NotificationSuccess extends Notification {

    public NotificationSuccess(String message) {
        super(message);
        addThemeVariants(NotificationVariant.LUMO_SUCCESS);
        setPosition(Notification.Position.BOTTOM_CENTER);
        setDuration(3000);
    }

}