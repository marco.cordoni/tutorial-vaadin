package com.store.ui.view;

import com.store.service.UserService;
import com.store.ui.component.UsersList;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import jakarta.annotation.security.RolesAllowed;

@RolesAllowed("ADMIN")
@Route(value = "users", layout = MainLayout.class)
@PageTitle("VDManga | Users")
public class UsersView extends VerticalLayout {

    public UsersView(UserService userService) {
        add(new UsersList(userService));
    }

}
