package com.store.ui.component;

import com.store.ui.utils.Constants;
import com.vaadin.flow.component.html.Image;

public class ResourceNotFoundImage extends Image {

    public ResourceNotFoundImage() {
        super(Constants.getPathImage("NotFound.jpg"), Constants.ERROR_IMAGE_NOT_FOUND);
    }

    public ResourceNotFoundImage(String width, String height) {
        super(Constants.getPathImage("NotFound.jpg"), Constants.ERROR_IMAGE_NOT_FOUND);
        setWidth(width);
        setHeight(height);
    }
}
