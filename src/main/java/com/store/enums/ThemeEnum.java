package com.store.enums;

import lombok.Getter;

@Getter
public enum ThemeEnum {
    LIGHT("light"),
    DARK("dark");

    private final String theme;

    ThemeEnum(String theme) {
        this.theme = theme;
    }

}
