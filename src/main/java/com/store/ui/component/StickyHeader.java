package com.store.ui.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.theme.lumo.LumoUtility;

public class StickyHeader extends Div {

    public StickyHeader(Component component) {
        getStyle().set("position", "sticky");
        getStyle().set("top", "0");
        getStyle().set("z-index", "10");
        getStyle().set("text-align", "center");

        addClassNames(LumoUtility.Background.BASE);

        setWidth("100%");

        component.getStyle().set("margin-top", "10px");
        component.getStyle().set("margin-bottom", "10px");

        add(component);
    }
}
