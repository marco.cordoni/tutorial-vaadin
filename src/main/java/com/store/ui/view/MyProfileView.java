package com.store.ui.view;

import com.store.dto.UserDTO;
import com.store.service.SecurityService;
import com.store.service.UserService;
import com.store.ui.component.MyProfileForm;
import com.store.ui.component.NotificationSuccess;
import com.store.ui.events.ButtonChangeUsernameEvent;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.lumo.LumoUtility;
import jakarta.annotation.security.PermitAll;

@PermitAll
@Route(value = "profile", layout = MainLayout.class)
@PageTitle("VDManga | My Profile")
public class MyProfileView extends VerticalLayout {

    private H1 welcome;
    private UserDTO loggedUser;
    private final Binder<UserDTO> binder;

    private UserService userService;

    public MyProfileView(SecurityService securityService, UserService userService) {
        this.userService = userService;

        // Get the authenticated user
        loggedUser = securityService.getAuthenticatedUser();

        welcome = new H1("Hi! " + loggedUser.getUsername());

        binder = new Binder<>(UserDTO.class);
        MyProfileForm formLayout = new MyProfileForm(loggedUser, binder);

        add(welcome, formLayout);

        add(createSaveButton());

        // Center the content
        setJustifyContentMode(JustifyContentMode.CENTER);
        setDefaultHorizontalComponentAlignment(Alignment.CENTER);
        getStyle().set("text-align", "center");
    }

    private Button createSaveButton() {
        Icon icon = new Icon(VaadinIcon.UPLOAD);
        icon.setSize("40px");
        Button button = new Button("SAVE", icon);
        button.addClassNames(
                LumoUtility.Margin.Top.MEDIUM,
                LumoUtility.FontSize.XLARGE,
                LumoUtility.Border.ALL
        );
        button.addClickListener(event -> {
            try {
                // update book
                binder.writeBean(loggedUser);
                userService.updateUser(loggedUser);

                NotificationSuccess notificationSuccess = new NotificationSuccess("User updated successfully");
                add(notificationSuccess);
                notificationSuccess.open();

                // update username in the header
                ComponentUtil.fireEvent(UI.getCurrent(), new ButtonChangeUsernameEvent(button, false));

                // update the welcome message
                welcome.setText("Hi! " + loggedUser.getUsername());
            } catch (ValidationException e) {
                throw new RuntimeException(e);
            }
        });

        return button;
    }

}