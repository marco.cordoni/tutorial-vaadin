package com.store.ui.utils;

import com.vaadin.flow.server.StreamResource;

import java.io.ByteArrayInputStream;
import java.util.Base64;

public class ResourceUtils {

    private static String TRANSPARENT_GIF_1PX = "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=";

    public static StreamResource createStreamResource(String title, byte[] image) {
        return new StreamResource(title, () -> new ByteArrayInputStream(image));
    }

    public static String getImageAsBase64(byte[] string) {
        String mimeType = "image/png";
        String htmlValue = null;

        if (string == null) {
            htmlValue = TRANSPARENT_GIF_1PX;
        } else {
            htmlValue = "data:" + mimeType + ";base64," + Base64.getEncoder().encodeToString(string);
        }
        
        return htmlValue;
    }


}
