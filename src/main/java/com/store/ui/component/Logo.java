package com.store.ui.component;

import com.store.ui.utils.Constants;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.theme.lumo.LumoUtility;

public class Logo extends H1 {

    public Logo() {
        super(Constants.HEADER_NAME);
        addClassNames(LumoUtility.FontSize.LARGE, LumoUtility.Margin.MEDIUM);
    }
}
