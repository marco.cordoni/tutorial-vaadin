package com.store.mapper;

import com.store.dto.UserDTO;
import com.store.entity.User;
import org.mapstruct.BeanMapping;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(
        uses = {RoleMapper.class},
        componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.WARN
)
public abstract class UserMapper {

    @Named("mapUserEntityToDTO")
    @Mapping(target = "role", source = "role", qualifiedByName = "mapRoleEntityToDTO")
    public abstract UserDTO mapUserEntityToDTO(User user);

    @Named("mapUserEntityListToDTOList")
    @IterableMapping(qualifiedByName = "mapUserEntityToDTO")
    public abstract List<UserDTO> mapUserEntityListToDTOList(List<User> userList);

    @Named("mapUserDTOToEntity")
    @Mapping(target = "role", ignore = true)
    @Mapping(target = "authorities", ignore = true)
    public abstract User mapUserDTOToEntity(UserDTO user);

    @Named("updateUser")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "authorities", ignore = true)
    @Mapping(target = "id", ignore = true)
    public abstract void updateUser(@MappingTarget User userTarget, User userSource);

}