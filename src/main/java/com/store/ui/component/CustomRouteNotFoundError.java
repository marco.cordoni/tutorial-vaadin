package com.store.ui.component;

import com.store.ui.view.WelcomeView;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.ErrorParameter;
import com.vaadin.flow.router.NotFoundException;
import com.vaadin.flow.router.RouteNotFoundError;
import jakarta.servlet.http.HttpServletResponse;

public class CustomRouteNotFoundError extends RouteNotFoundError {

    @Override
    public int setErrorParameter(final BeforeEnterEvent event, final ErrorParameter<NotFoundException> parameter) {
        event.forwardTo(WelcomeView.class);
        return HttpServletResponse.SC_NOT_FOUND;
    }
}