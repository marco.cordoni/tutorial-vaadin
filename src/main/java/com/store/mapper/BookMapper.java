package com.store.mapper;

import com.store.dto.BookDTO;
import com.store.entity.Book;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(
        componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.WARN
)
public abstract class BookMapper {

    @Named("mapBookEntityToDTO")
    public abstract BookDTO mapBookEntityToDTO(Book book);

    @Named("mapBookEntityToDTOWithoutImage")
    @Mapping(target = "image", ignore = true)
    public abstract BookDTO mapBookEntityToDTOWithoutImage(Book book);

    @Named("mapBookDTOToEntity")
    public abstract Book mapBookDTOToEntity(BookDTO bookDTO);

    @Named("updateBookNoImage")
    @Mapping(target = "image", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "isbn", ignore = true)
    public abstract void updateBookNoImage(@MappingTarget Book bookTarget, Book bookSource);

    @Named("mapBookEntityListToDTOList")
    @IterableMapping(qualifiedByName = "mapBookEntityToDTO")
    public abstract List<BookDTO> mapBookEntityListToDTOList(List<Book> bookList);

    @Named("mapBookEntityListToDTOListWithoutImage")
    @IterableMapping(qualifiedByName = "mapBookEntityToDTOWithoutImage")
    public abstract List<BookDTO> mapBookEntityListToDTOListWithoutImage(List<Book> bookList);
}