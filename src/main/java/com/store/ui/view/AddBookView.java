package com.store.ui.view;

import com.store.service.BookService;
import com.store.service.SecurityService;
import com.store.ui.component.AddBookForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import jakarta.annotation.security.RolesAllowed;
import org.springframework.beans.factory.annotation.Autowired;

@RolesAllowed("ADMIN")
@Route(value = "book/add", layout = MainLayout.class)
@PageTitle("VDManga | Add Book")
public class AddBookView extends VerticalLayout {

    @Autowired
    private BookService bookService;

    @Autowired
    private SecurityService securityService;

    public AddBookView(BookService bookService) {
        // Add the form
        add(new AddBookForm(bookService));

        // Center the content
        setSizeFull();
        setJustifyContentMode(JustifyContentMode.CENTER);
        setDefaultHorizontalComponentAlignment(Alignment.CENTER);
    }

}