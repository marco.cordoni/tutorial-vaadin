package com.store.ui.view;

import com.store.ui.utils.Constants;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import com.vaadin.flow.theme.lumo.LumoUtility;

@AnonymousAllowed
@PageTitle("VDManga | Welcome")
@Route(value = "", layout = MainLayout.class)
public class WelcomeView extends VerticalLayout {

    public WelcomeView() {
        Image img = new Image(Constants.getPathImage("LogoVDManga.png"), Constants.ERROR_IMAGE_NOT_FOUND);
        img.setWidth("400px");
        add(img);

        H2 header = new H2("Welcome to VDManga!");
        header.addClassNames(LumoUtility.Margin.Top.SMALL, LumoUtility.Margin.Bottom.SMALL);
        add(header);
        add(new Paragraph("Enjoy your visit"));

        setSizeFull();
        setJustifyContentMode(JustifyContentMode.CENTER);
        setDefaultHorizontalComponentAlignment(Alignment.CENTER);
        getStyle().set("text-align", "center");
    }
}
