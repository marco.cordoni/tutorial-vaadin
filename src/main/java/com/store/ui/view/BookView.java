package com.store.ui.view;

import com.store.service.BookService;
import com.store.service.SecurityService;
import com.store.ui.component.BookDetail;
import com.store.ui.component.ResourceNotFoundImage;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import com.vaadin.flow.theme.lumo.LumoUtility;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@AnonymousAllowed
@Route(value = "book/:isbn", layout = MainLayout.class)
@PageTitle("VDManga | Book")
public class BookView extends VerticalLayout implements BeforeEnterObserver {

    @Autowired
    private BookService bookService;

    @Autowired
    private SecurityService securityService;

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        Optional<String> isbnOpt = event.getRouteParameters().get("isbn");

        isbnOpt.ifPresentOrElse(
                isbn -> {
                    bookService.findByIsbn(isbn).ifPresentOrElse(
                            book -> add(new BookDetail(bookService, securityService, book)),
                            () -> event.forwardTo(BooksView.class)
                    );

                    // Add margin to the top of the layout
                    addClassName(LumoUtility.Margin.Top.LARGE);

                    // Center the content
                    setJustifyContentMode(JustifyContentMode.CENTER);
                    setDefaultHorizontalComponentAlignment(Alignment.CENTER);
                },
                () -> event.forwardTo(BooksView.class)
        );
    }

    private HorizontalLayout getResourceNotFoundImage() {
        return new HorizontalLayout(new ResourceNotFoundImage("500px", "400px"));
    }
}