package com.store.ui.component;

import com.store.service.SecurityService;
import com.store.ui.utils.Constants;
import com.store.ui.view.AddBookView;
import com.store.ui.view.BooksView;
import com.store.ui.view.UsersView;
import com.store.ui.view.WelcomeView;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.theme.lumo.LumoUtility;

public class Drawer extends VerticalLayout implements BeforeEnterObserver {

    private final SecurityService securityService;
    private boolean areLoggedRoutesAdded = false;

    public Drawer(SecurityService securityService) {
        this.securityService = securityService;

        addClassNames(LumoUtility.Padding.Vertical.LARGE, LumoUtility.Padding.Horizontal.LARGE, LumoUtility.FontSize.LARGE, LumoUtility.AlignItems.CENTER);
        add(new RouterLink(Constants.SIDE_BAR_WELCOME, WelcomeView.class));
        add(new RouterLink(Constants.SIDE_BAR_BOOKS, BooksView.class));
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        if (!areLoggedRoutesAdded && securityService.isAdmin()) {
            areLoggedRoutesAdded = true;
            add(new RouterLink(Constants.SIDE_BAR_USERS, UsersView.class));
            add(new RouterLink(Constants.SIDE_BAR_ADD_BOOK, AddBookView.class));
        }
    }
}
