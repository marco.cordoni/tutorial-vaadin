package com.store.ui.component;

import com.store.dto.UserDTO;
import com.store.service.UserService;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class UsersList extends VerticalLayout {

    public UsersList(UserService userService) {
        Grid<UserDTO> grid = new Grid<>();

        grid.addColumn(UserDTO::getId).setHeader("Id").setAutoWidth(true);
        grid.addColumn(UserDTO::getUsername).setHeader("Username").setAutoWidth(true);
        grid.addColumn(UserDTO::getName).setHeader("Name").setAutoWidth(true);
        grid.addColumn(UserDTO::getSurname).setHeader("Surname").setAutoWidth(true);
        grid.addColumn(UserDTO::getEmail).setHeader("Email").setAutoWidth(true);
        grid.addColumn(UserDTO::getCountry).setHeader("Country").setAutoWidth(true);
        grid.addColumn(userDTO -> userDTO.getRole().getName()).setHeader("Role").setAutoWidth(true);
        grid.addColumn(UserDTO::isEnabled).setHeader("Enabled").setAutoWidth(true);

        grid.setItems(userService.findAll());

        add(grid);
    }

}
