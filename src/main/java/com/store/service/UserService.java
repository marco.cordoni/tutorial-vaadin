package com.store.service;

import com.store.dto.UserDTO;
import com.store.entity.User;
import com.store.mapper.UserMapper;
import com.store.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private final UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository
                .findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));
    }

    public List<UserDTO> findAll() {
        return userMapper.mapUserEntityListToDTOList(userRepository.findAll());
    }

    public void updateUser(UserDTO userDTO) {
        Optional<User> userTargetOpt = userRepository.findById(userDTO.getId());

        if (userTargetOpt.isPresent()) {
            User userTarget = userTargetOpt.get();
            User userSource = userMapper.mapUserDTOToEntity(userDTO);
            userMapper.updateUser(userTarget, userSource);
            userRepository.save(userTarget);
        }
    }

    public UserDTO findById(UUID id) {
        Optional<User> user = userRepository.findById(id);
        return user.map(userMapper::mapUserEntityToDTO).orElse(null);
    }
}