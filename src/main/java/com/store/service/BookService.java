package com.store.service;

import com.store.dto.BookDTO;
import com.store.entity.Book;
import com.store.mapper.BookMapper;
import com.store.repository.BookRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private BookMapper bookMapper;

    public List<BookDTO> findAll() {
        return bookMapper.mapBookEntityListToDTOList(bookRepository.findAll());
    }

    public List<BookDTO> findAllWithoutImage() {
        return bookMapper.mapBookEntityListToDTOListWithoutImage(bookRepository.findAll());
    }

    public Optional<BookDTO> findByIsbn(String isbn) {
        Optional<Book> bookOpt = bookRepository.findByIsbn(isbn);
        return bookOpt.map(book -> bookMapper.mapBookEntityToDTO(book));
    }

    @Transactional
    public void deleteByIsbn(String isbn) {
        bookRepository.deleteByIsbn(isbn);
    }

    @Transactional
    public void createBook(BookDTO bookDTO) {
        bookRepository.save(bookMapper.mapBookDTOToEntity(bookDTO));
    }

    @Transactional
    public void updateBookNoImage(BookDTO bookDTO) {
        Optional<Book> bookTargetOpt = bookRepository.findByIsbn(bookDTO.getIsbn());

        if (bookTargetOpt.isPresent()) {
            Book bookTarget = bookTargetOpt.get();
            Book bookSource = bookMapper.mapBookDTOToEntity(bookDTO);
            bookMapper.updateBookNoImage(bookTarget, bookSource);
            bookRepository.save(bookTarget);
        }
    }

}
