package com.store.ui.component;

import com.store.enums.BooksViewEnum;
import com.store.ui.view.BooksView;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.router.RouteParameters;
import com.vaadin.flow.theme.lumo.LumoUtility;
import org.vaadin.addons.taefi.component.ToggleButtonGroup;

import java.util.List;

public class ChangeBooksViewToggle extends ToggleButtonGroup<BooksViewEnum> {

    public ChangeBooksViewToggle() {
        setItems(List.of(BooksViewEnum.values()));
        setItemIconGenerator(align -> switch (align) {
            case CARDS -> VaadinIcon.GRID.create();
            case LIST -> VaadinIcon.LINES_LIST.create();
        });
        addValueChangeListener(event ->
                UI.getCurrent().navigate(
                        BooksView.class,
                        new RouteParameters("type", event.getValue().getValue())
                )
        );
        setValue(BooksViewEnum.CARDS);
        setToggleable(false);
    }

    public void setThemeVariant(BooksViewEnum type) {
        setValue(type);
    }
}
