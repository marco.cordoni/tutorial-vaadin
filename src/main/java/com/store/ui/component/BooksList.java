package com.store.ui.component;

import com.store.dto.BookDTO;
import com.store.enums.BooksViewEnum;
import com.store.service.BookService;
import com.store.service.SecurityService;
import com.store.ui.utils.ResourceUtils;
import com.store.ui.view.BookView;
import com.store.ui.view.BooksView;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.router.RouteParameters;
import org.vaadin.klaudeta.PaginatedGrid;

public class BooksList extends VerticalLayout {

    public BooksList(BookService bookService, SecurityService securityService) {
        PaginatedGrid<BookDTO, ?> grid = new PaginatedGrid<>();

        grid.addColumn(createCoverRenderer()).setHeader("Cover").setAutoWidth(true).setFlexGrow(0);
        grid.addColumn(BookDTO::getTitle).setHeader("Title").setAutoWidth(true);
        grid.addColumn(BookDTO::getAuthor).setHeader("Author").setAutoWidth(true);
        grid.addColumn(BookDTO::getIsbn).setHeader("Isbn").setAutoWidth(true);
        grid.addColumn(createPriceField()).setHeader("Price").setAutoWidth(true);
        grid.addColumn(createEditEvent(bookService, securityService)).setHeader("Edit").setTextAlign(ColumnTextAlign.CENTER);
        grid.addColumn(createDeleteEvent(bookService, securityService)).setHeader("Delete").setTextAlign(ColumnTextAlign.CENTER);

        grid.addItemClickListener(book ->
                UI.getCurrent().navigate(
                        BookView.class,
                        new RouteParameters("isbn", book.getItem().getIsbn())
                )
        );

        grid.setItems(bookService.findAll());

        // Sets the max number of items to be rendered on the grid for each page
        grid.setPageSize(8);

        // Sets how many pages should be visible on the pagination before and/or after the current selected page
        grid.setPaginatorSize(3);

        add(grid);
    }

    private static ComponentRenderer<Button, BookDTO> createEditEvent(BookService bookService, SecurityService securityService) {
        return new ComponentRenderer<>(Button::new, (button, book) -> {
            button.addThemeVariants(
                    ButtonVariant.LUMO_ICON,
                    ButtonVariant.LUMO_ERROR,
                    ButtonVariant.LUMO_TERTIARY
            );
            button.addClickListener(e -> {
                // edit book
                UI.getCurrent().navigate(
                        BookView.class,
                        new RouteParameters("isbn", book.getIsbn())
                );
            });
            Icon icon = new Icon(VaadinIcon.PENCIL);
            icon.setSize("30px");
            button.setIcon(icon);
            button.setEnabled(securityService.isAdmin());
        });
    }

    private ComponentRenderer<Button, BookDTO> createDeleteEvent(BookService bookService, SecurityService securityService) {
        return new ComponentRenderer<>(Button::new, (button, book) -> {
            button.addThemeVariants(
                    ButtonVariant.LUMO_ICON,
                    ButtonVariant.LUMO_ERROR,
                    ButtonVariant.LUMO_TERTIARY
            );
            button.addClickListener(e -> {
                bookService.deleteByIsbn(book.getIsbn());
                UI.getCurrent().navigate(
                        BooksView.class,
                        new RouteParameters("type", BooksViewEnum.LIST.getValue())
                );
            });
            Icon icon = new Icon(VaadinIcon.TRASH);
            icon.setSize("30px");
            button.setIcon(icon);
            button.setEnabled(securityService.isAdmin());
        });
    }

    private Renderer<BookDTO> createPriceField() {
        return LitRenderer.<BookDTO>of("${item.price}")
                .withProperty("price", book -> String.format("%.2f €", book.getPrice()));
    }

    private Renderer<BookDTO> createCoverRenderer() {
        return LitRenderer.<BookDTO>of(
                        "<div><img style='height: 80px; width: 60px;' src=${item.image} alt=${item.title}></div>"
                )
                .withProperty("image", item -> ResourceUtils.getImageAsBase64(item.getImage()))
                .withProperty("name", BookDTO::getTitle);
    }

}
