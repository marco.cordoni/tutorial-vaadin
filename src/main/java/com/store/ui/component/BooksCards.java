package com.store.ui.component;

import com.store.dto.BookDTO;
import com.store.service.BookService;
import com.store.service.SecurityService;
import com.store.ui.view.BookView;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.RouteParameters;
import com.vaadin.flow.theme.lumo.LumoUtility;

public class BooksCards extends Div {

    private CardGrid grid;
    private final BookService bookService;
    private final SecurityService securityService;

    public BooksCards(BookService bookService, SecurityService securityService) {
        this.bookService = bookService;
        this.securityService = securityService;
        initGrid();
    }

    private void initGrid() {
        grid = new CardGrid();

        bookService.findAll().forEach(book -> {
            VerticalLayout cardLayout = new VerticalLayout();
            cardLayout.add(new Card(book.getTitle(), new BookImage(book)));

            if (securityService.isAdmin()) {
                cardLayout.add(createButtons(book));
            }

            grid.add(cardLayout);
        });
        add(grid);
    }

    private Div createButtons(BookDTO book) {
        Div ctaButtons = new Div();

        Button editButton = createEditButton(book);
        Button deleteButton = createDeleteButton(book);

        ctaButtons.addClassNames(LumoUtility.AlignSelf.CENTER);
        ctaButtons.add(editButton, deleteButton);

        return ctaButtons;
    }

    private Button createEditButton(BookDTO book) {
        Button button = new Button();
        button.addClassName(LumoUtility.Margin.NONE);
        button.addThemeVariants(
                ButtonVariant.LUMO_ICON,
                ButtonVariant.LUMO_ERROR,
                ButtonVariant.LUMO_TERTIARY
        );
        button.addClickListener(event -> {
            // edit book
            UI.getCurrent().navigate(
                    BookView.class,
                    new RouteParameters("isbn", book.getIsbn())
            );
        });
        button.setIcon(new Icon(VaadinIcon.PENCIL));
        return button;
    }

    private Button createDeleteButton(BookDTO book) {
        Button button = new Button();
        button.addClassName(LumoUtility.Margin.NONE);
        button.addThemeVariants(
                ButtonVariant.LUMO_ICON,
                ButtonVariant.LUMO_ERROR,
                ButtonVariant.LUMO_TERTIARY
        );
        button.addClickListener(event -> {
            // delete book
            bookService.deleteByIsbn(book.getIsbn());

            // reload the grid
            remove(grid);
            initGrid();
        });
        button.setIcon(new Icon(VaadinIcon.TRASH));
        return button;
    }
}
