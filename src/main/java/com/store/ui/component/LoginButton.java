package com.store.ui.component;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;

public class LoginButton extends Button {

    public LoginButton() {
        super("Log in");
        MyLoginForm dialogLayout = new MyLoginForm();

        Dialog dialog = new Dialog();
        dialog.add(dialogLayout);
        dialog.setDraggable(true);

        addClickListener(event -> dialog.open());

        Button closeButton = new Button("Close");
        closeButton.addClickListener(e -> dialog.close());

        dialogLayout.add(closeButton);
    }
}
