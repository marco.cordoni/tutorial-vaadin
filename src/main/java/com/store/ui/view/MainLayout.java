package com.store.ui.view;

import com.store.service.SecurityService;
import com.store.service.UserService;
import com.store.ui.component.ChangeThemeToggle;
import com.store.ui.component.Drawer;
import com.store.ui.component.LoginButton;
import com.store.ui.component.Logo;
import com.store.ui.component.LogoutButton;
import com.store.ui.component.MyFooter;
import com.store.ui.utils.Constants;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.theme.lumo.LumoUtility;

public class MainLayout extends AppLayout {

    private Button loginOrLogoutButton;
    private RouterLink myProfile;

    public MainLayout(SecurityService securityService, UserService userService) {
        createHeader(securityService, userService);
        createDrawer(securityService);
        createFooter();
    }

    private void createHeader(SecurityService securityService, UserService userService) {
        addToNavbar(createHorizontalLayout(new DrawerToggle(), new Logo()));

        loginOrLogoutButton = new LoginButton();

        if (securityService.isAuthenticated()) {
            myProfile = new RouterLink(Constants.MY_PROFILE, MyProfileView.class);
            myProfile.addClassNames(LumoUtility.Flex.NONE, LumoUtility.Margin.Right.MEDIUM);

            loginOrLogoutButton = new LogoutButton(securityService, userService);
        }

        addToNavbar(myProfile, loginOrLogoutButton, new ChangeThemeToggle());
    }

    private HorizontalLayout createHorizontalLayout(Component... children) {
        HorizontalLayout horizontalLayout = new HorizontalLayout(children);
        horizontalLayout.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        horizontalLayout.setWidthFull();
        horizontalLayout.addClassNames(LumoUtility.Padding.Vertical.NONE, LumoUtility.Padding.Horizontal.MEDIUM);
        return horizontalLayout;
    }

    private void createDrawer(SecurityService securityService) {
        addToDrawer(new Drawer(securityService));
    }

    private void createFooter() {
        addToNavbar(new MyFooter());
    }

}