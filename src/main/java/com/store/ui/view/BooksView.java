package com.store.ui.view;

import com.store.enums.BooksViewEnum;
import com.store.service.BookService;
import com.store.service.SecurityService;
import com.store.ui.component.BooksCards;
import com.store.ui.component.BooksList;
import com.store.ui.component.ChangeBooksViewToggle;
import com.store.ui.component.StickyHeader;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@AnonymousAllowed
@Route(value = "books/:type?", layout = MainLayout.class)
@PageTitle("VDManga | Books")
public class BooksView extends VerticalLayout implements BeforeEnterObserver {

    @Autowired
    private BookService bookService;

    @Autowired
    private SecurityService securityService;

    private ChangeBooksViewToggle changeBooksViewToggle;

    private Component booksView;

    public BooksView() {
        changeBooksViewToggle = new ChangeBooksViewToggle();
        add(new StickyHeader(changeBooksViewToggle));
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        Optional<String> typeOpt = event.getRouteParameters().get("type");

        if (booksView != null) {
            remove(booksView);
        }

        if (typeOpt.isPresent() && typeOpt.get().equals("list")) {
            changeBooksViewToggle.setThemeVariant(BooksViewEnum.valueOf(typeOpt.get().toUpperCase()));
            booksView = new BooksList(bookService, securityService);
        } else {
            booksView = new BooksCards(bookService, securityService);
        }

        add(booksView);

        setJustifyContentMode(JustifyContentMode.CENTER);
        setDefaultHorizontalComponentAlignment(Alignment.CENTER);
    }

}
