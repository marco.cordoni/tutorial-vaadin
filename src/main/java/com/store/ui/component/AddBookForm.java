package com.store.ui.component;

import com.store.dto.BookDTO;
import com.store.service.BookService;
import com.store.ui.view.BooksView;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MultiFileMemoryBuffer;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.router.RouteParameters;
import com.vaadin.flow.theme.lumo.LumoUtility;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;

public class AddBookForm extends Div {

    private final BookService bookService;

    private Upload upload;
    private byte[] image;

    private Binder<BookDTO> binder;

    public AddBookForm(BookService bookService) {
        this.bookService = bookService;

        setMaxWidth("50%");

        add(uploadImageForm());
        add(createFormFields());
        add(createSaveButton(), createResetButton());

        addClassNames(LumoUtility.TextAlignment.CENTER);
    }

    private Upload uploadImageForm() {
        MultiFileMemoryBuffer buffer = new MultiFileMemoryBuffer();
        upload = new Upload(buffer);

        upload.addSucceededListener(event -> {
            String fileName = event.getFileName();
            InputStream inputStream = buffer.getInputStream(fileName);
            try {
                image = IOUtils.toByteArray(inputStream);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        return upload;
    }

    private FormLayout createFormFields() {
        TextField title = new TextField("Title");
        title.setRequired(true);

        TextField author = new TextField("Author");
        author.setRequired(true);

        TextField isbn = new TextField("ISBN");
        isbn.setRequired(true);

        NumberField price = new NumberField("Price €");
        price.setRequired(true);
        price.setValue(0d);

        TextArea description = new TextArea("Description");
        description.setRequired(true);

        binder = new Binder<>(BookDTO.class);
        binder.forField(title)
                .asRequired("Every book must have a title")
                .bind(BookDTO::getTitle, BookDTO::setTitle);

        binder.forField(author)
                .asRequired("Every book must have an author")
                .bind(BookDTO::getAuthor, BookDTO::setAuthor);

        binder.forField(isbn)
                .asRequired("Every book must have an isbn")
                .bind(BookDTO::getIsbn, BookDTO::setIsbn);

        binder.forField(price)
                .asRequired("Every book must have a price")
                .bind(
                        book -> book.getPrice() == null ? 0d : book.getPrice().doubleValue(),
                        (bookDTO, aDouble) -> bookDTO.setPrice(BigDecimal.valueOf(aDouble))
                );

        binder.forField(description)
                .asRequired("Every book must have a description")
                .bind(BookDTO::getDescription, BookDTO::setDescription);

        // Add the form fields to the page
        FormLayout formLayout = new FormLayout();
        formLayout.add(title, author, isbn, price, description);

        // Setup form layout
        formLayout.setResponsiveSteps(
                // Use one column by default
                new FormLayout.ResponsiveStep("0", 1)
        );
        formLayout.setMaxWidth("500px");
        return formLayout;
    }


    private Button createSaveButton() {
        Icon icon = new Icon(VaadinIcon.UPLOAD);
        Button button = new Button("SAVE", icon);
        button.addClassName(LumoUtility.Margin.NONE);
        button.addThemeVariants(
                ButtonVariant.LUMO_ICON,
                ButtonVariant.LUMO_ERROR,
                ButtonVariant.LUMO_TERTIARY
        );
        button.addClickListener(event -> {
            if (image != null) {
                upload.getStyle().set("border", "inherit");

                try {
                    // save book
                    BookDTO book = new BookDTO();
                    binder.writeBean(book);

                    book.setImage(image);
                    bookService.createBook(book);
                } catch (ValidationException e) {
                    throw new RuntimeException(e);
                }
                UI.getCurrent().navigate(BooksView.class, new RouteParameters());
            } else {
                upload.getStyle().set("border", "1px solid red");
            }
        });
        return button;
    }

    private Button createResetButton() {
        Icon icon = new Icon(VaadinIcon.TRASH);
        Button button = new Button("RESET", icon);
        button.addClassName(LumoUtility.Margin.NONE);
        button.addThemeVariants(
                ButtonVariant.LUMO_ICON,
                ButtonVariant.LUMO_ERROR,
                ButtonVariant.LUMO_TERTIARY
        );
        button.addClickListener(event -> {
            binder.readBean(new BookDTO());
            image = null;
        });
        return button;
    }

}
