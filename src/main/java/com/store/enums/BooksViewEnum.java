package com.store.enums;

import lombok.Getter;

@Getter
public enum BooksViewEnum {
    CARDS("cards"),
    LIST("list");

    private final String value;

    BooksViewEnum(String value) {
        this.value = value;
    }
}
