package com.store.ui.utils;

public class Constants {

    // Configuration fields
    public static final String ERROR_IMAGE_NOT_FOUND = "Image not found";
    public static final String IMAGE_BASE_PATH = "images/";
    public static final String MY_PROFILE = "My Profile";

    // Layout fields
    public static final String HEADER_NAME = "VDManga Store";
    public static final String SIDE_BAR_WELCOME = "Welcome";
    public static final String SIDE_BAR_BOOKS = "Books";
    public static final String SIDE_BAR_USERS = "Users";
    public static final String SIDE_BAR_ADD_BOOK = "Add new Book";

    public static String getPathImage(String imageName) {
        return IMAGE_BASE_PATH + imageName;
    }

}
