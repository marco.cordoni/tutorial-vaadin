package com.store.mapper;

import com.store.dto.RoleDTO;
import com.store.dto.UserDTO;
import com.store.entity.Role;
import com.store.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

@Mapper(
        componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.WARN
)
public abstract class RoleMapper {

    @Named("mapRoleEntityToDTO")
    public abstract RoleDTO mapRoleEntityToDTO(Role role);

}