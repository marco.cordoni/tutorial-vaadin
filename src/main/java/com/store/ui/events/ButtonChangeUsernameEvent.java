package com.store.ui.events;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.button.Button;

public class ButtonChangeUsernameEvent extends ComponentEvent<Button> {

    public ButtonChangeUsernameEvent(Button source, boolean fromClient) {
        super(source, fromClient);
    }
}