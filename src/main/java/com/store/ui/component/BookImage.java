package com.store.ui.component;

import com.store.dto.BookDTO;
import com.store.ui.utils.ResourceUtils;
import com.store.ui.view.BookView;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.router.RouteParameters;
import com.vaadin.flow.theme.lumo.LumoUtility;

public class BookImage extends Image {

    public BookImage(BookDTO book) {
        super(ResourceUtils.createStreamResource(book.getTitle(), book.getImage()), book.getTitle());
        setMaxWidth("200px");
        setMaxHeight("200px");
        addClassNames(LumoUtility.Flex.SHRINK_NONE, LumoUtility.MinWidth.FULL, LumoUtility.MinHeight.FULL);
        addTransitionOnMouseOver();
        addRedirectToBookPageOnClick(book);
    }

    private void addTransitionOnMouseOver() {
        // Transition speed
        getStyle().set("transition", "transform .2s");

        // Zoom on mouse over
        getElement().addEventListener("mouseover", event -> {
            getStyle().set("transform", "scale(1.1)");
        });

        // Return to original size when mouse is no longer over the image
        getElement().addEventListener("mouseout", event -> {
            getStyle().set("transform", "scale(1)");
        });
    }

    private void addRedirectToBookPageOnClick(BookDTO book) {
        // redirect to book page when the image is clicked
        addClickListener(event ->
                UI.getCurrent().navigate(
                        BookView.class,
                        new RouteParameters("isbn", book.getIsbn())
                )
        );
    }

}
