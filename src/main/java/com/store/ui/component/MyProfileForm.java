package com.store.ui.component;

import com.store.dto.UserDTO;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class MyProfileForm extends Div {

    public MyProfileForm(UserDTO user, Binder<UserDTO> binder) {
        // Setup the form fields
        TextField username = new TextField("Username");
        username.setValue(user.getUsername());

        TextField role = new TextField("Role");
        role.setValue(user.getRole().getName());
        role.setEnabled(false);

        TextField name = new TextField("Name");
        name.setValue(user.getName());

        TextField surname = new TextField("Surname");
        surname.setValue(user.getSurname());

        EmailField email = new EmailField("Email");
        email.setValue(user.getEmail());

        TextField country = new TextField("Country");
        country.setValue(user.getCountry());

        // bind field
        binder.forField(username)
                .asRequired("Every user must have a username")
                .bind(UserDTO::getUsername, UserDTO::setUsername);

        binder.forField(role)
                .bind(userDTO -> userDTO.getRole().getName(), (userDTO, s) -> userDTO.setRole(userDTO.getRole()));

        binder.forField(name)
                .asRequired("Every user must have a name")
                .bind(UserDTO::getName, UserDTO::setName);

        binder.forField(surname)
                .asRequired("Every user must have a surname")
                .bind(UserDTO::getSurname, UserDTO::setSurname);

        binder.forField(email)
                .asRequired("Every user must have an email")
                .bind(UserDTO::getEmail, UserDTO::setEmail);

        binder.forField(country)
                .asRequired("Every user must have a country")
                .bind(UserDTO::getCountry, UserDTO::setCountry);


        // Add the form fields to the page
        FormLayout formLayout = new FormLayout();
        formLayout.add(username, role, name, surname, email, country);

        // Setup form layout
        formLayout.setResponsiveSteps(
                // Use one column by default
                new FormLayout.ResponsiveStep("0", 1)
        );
        formLayout.setMaxWidth("500px");

        add(formLayout);
    }
}
