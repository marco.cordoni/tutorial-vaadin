package com.store.service;

import com.store.dto.UserDTO;
import com.store.entity.User;
import com.store.mapper.UserMapper;
import com.vaadin.flow.spring.security.AuthenticationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SecurityService {

    @Autowired
    private UserMapper userMapper;

    private final AuthenticationContext authenticationContext;

    public SecurityService(AuthenticationContext authenticationContext) {
        this.authenticationContext = authenticationContext;
    }

    public UserDTO getAuthenticatedUser() {
        Optional<User> userDetailsOptional = authenticationContext.getAuthenticatedUser(User.class);
        return userDetailsOptional.map(user -> userMapper.mapUserEntityToDTO(user)).orElse(null);
    }

    public boolean isAuthenticated() {
        return getAuthenticatedUser() != null;
    }

    public void logout() {
        authenticationContext.logout();
    }

    public boolean isAdmin() {
        UserDTO userDTO = getAuthenticatedUser();
        return userDTO != null && userDTO.getRole().getName().equals("ADMIN");
    }
}